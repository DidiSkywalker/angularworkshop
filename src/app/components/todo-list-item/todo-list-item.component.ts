import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import TodoList from '../../models/TodoList';
import {TodoListService} from '../../services/todo-list.service';

@Component({
  selector: 'app-todo-list-item',
  templateUrl: './todo-list-item.component.html',
  styleUrls: ['./todo-list-item.component.css']
})
export class TodoListItemComponent implements OnInit {
  @Input() public todoList: TodoList;
  @Output() public listDeleted = new EventEmitter<TodoList>();
  public completedItemCount: number;
  public itemCount: number;
  constructor(private todoService: TodoListService) { }

  ngOnInit(): void {
    this.itemCount = this.todoList.toDos.length;
    this.completedItemCount = this.todoList.toDos.filter(item => item.completed).length;
  }

  onDelete(): void {
    this.todoService.deleteTodoList(this.todoList.id).subscribe(list => {
      // nop
    });
    this.listDeleted.emit(this.todoList);
  }

}
