import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import TodoItem from '../../models/TodoItem';
import TodoList from '../../models/TodoList';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  @Input() todoList: TodoList;
  @Output() todoDeleted = new EventEmitter<TodoItem>();
  constructor() { }

  ngOnInit(): void {
  }

  onTodoDeleted(todoItem: TodoItem): void {
    this.todoDeleted.emit(todoItem);
  }

}
