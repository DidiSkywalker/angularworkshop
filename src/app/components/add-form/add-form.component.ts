import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {TodoListService} from '../../services/todo-list.service';
import TodoItem from '../../models/TodoItem';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent implements OnInit {
  @Input() public label: string;
  @Input() public placeholder: string;
  @Input() public buttonText: string;
  @Output() public add = new EventEmitter<string>();
  public titleForm = new FormGroup({
    title: new FormControl('')
  });
  constructor(private todoService: TodoListService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.add.emit(this.titleForm.value.title);
    this.titleForm.reset('');
  }

}
