import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {ID_PARAM, ListPageComponent} from './pages/list-page/list-page.component';

const routes: Routes = [
  {
    // Paths to /list/{id} get routed to the ListPageComponent
    path: `list/:${ID_PARAM}`,
    component: ListPageComponent
  },
  {
    // Paths to the root / get routed to the HomePageComponent
    path: '',
    component: HomePageComponent
  },
  {
    // Any other paths get redirected to the root
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
