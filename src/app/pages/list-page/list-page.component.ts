import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TodoListService} from '../../services/todo-list.service';
import TodoList from '../../models/TodoList';
import TodoItem from '../../models/TodoItem';

export const ID_PARAM = 'id';

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.css']
})
export class ListPageComponent implements OnInit {

  private listId: string;
  public list: TodoList;
  constructor(private route: ActivatedRoute, private todoListService: TodoListService) {
    this.route.params.subscribe(params => {
      this.listId = params[ID_PARAM];
    });
  }

  ngOnInit(): void {
    this.todoListService.getTodoList(this.listId).subscribe(todoList => (this.list = todoList));
  }

  onTodoAdded(title: string): void {
    this.todoListService.addTodoItem(this.listId, title).subscribe(todoItem => {
      this.list.toDos.push(todoItem);
    });
  }

  onTodoDeleted(todoItem: TodoItem): void {
    this.list.toDos = this.list.toDos.filter(item => item.id !== todoItem.id);
  }

}
