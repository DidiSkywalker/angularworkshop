import { Component, OnInit } from '@angular/core';
import TodoList from '../../models/TodoList';
import {TodoListService} from '../../services/todo-list.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  public todoLists: TodoList[];
  constructor(private todoService: TodoListService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.todoService.getTodoLists().subscribe(lists => {
      this.todoLists = lists;
    });
  }

  onListDeleted(todoList: TodoList): void {
    this.todoLists = this.todoLists.filter(list => list.id !== todoList.id);
    this.snackBar.open('List deleted', '', {
      duration: 1500
    });
  }

  onListCreated(name: string): void {
    this.todoService.addTodoList(name).subscribe(todoList => {
      this.todoLists.push(todoList);
    });
  }

}
