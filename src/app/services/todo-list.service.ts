import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import TodoList from '../models/TodoList';
import TodoItem from '../models/TodoItem';

const BASE_URL = 'http://localhost:8080/api';

@Injectable({
  providedIn: 'root'
})
export class TodoListService {

  constructor(private http: HttpClient) {
  }

  /**
   * Get all TodoLists.
   */
  public getTodoLists(): Observable<TodoList[]> {
    return this.http.get<TodoList[]>(`${BASE_URL}/list`);
  }

  /**
   * Get a specific TodoList by id.
   * @param id The list's unique id
   */
  public getTodoList(id: string): Observable<TodoList> {
    return this.http.get<TodoList>(`${BASE_URL}/list/${id}`);
  }

  /**
   * Create a new TodoList.
   * @param name The name of the new list
   */
  public addTodoList(name: string): Observable<TodoList> {
    return this.http.post<TodoList>(`${BASE_URL}/list`, {
      name
    });
  }

  /**
   * Delete a TodoList.
   * @param id The list's unique id
   */
  public deleteTodoList(id: string): Observable<TodoList> {
    return this.http.delete<TodoList>(`${BASE_URL}/list/${id}`);
  }

  /**
   * Update a TodoItem's completed value.
   * @param listId The list's unique id
   * @param item The updated item
   */
  public updateTodoItem(listId: string, item: TodoItem): Observable<TodoItem> {
    return this.http.put<TodoItem>(`${BASE_URL}/list/${listId}/items/${item.id}/completed`, {
      completed: item.completed
    });
  }

  /**
   * Add a new TodoItem to a list.
   * @param listId The list's unique id
   * @param title The new item's title
   */
  public addTodoItem(listId: string, title: string): Observable<TodoItem> {
    return this.http.post<TodoItem>(`${BASE_URL}/list/${listId}/items`, {
      title
    });
  }

  /**
   * Delete a TodoItem.
   * @param listId The list's unique id
   * @param itemId The item's unique id
   */
  public deleteTodoItem(listId: string, itemId: string): Observable<TodoItem> {
    return this.http.delete<TodoItem>(`${BASE_URL}/list/${listId}/items/${itemId}`);
  }
}
