import TodoItem from './TodoItem';

export default class TodoList {
  id: string;
  name: string;
  toDos: TodoItem[];

  constructor(id: string, name: string, items: TodoItem[]) {
    this.id = id;
    this.name = name;
    this.toDos = items;
  }
}
