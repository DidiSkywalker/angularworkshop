export default class TodoItem {
  id: string;
  title: string;
  completed: boolean;
}
