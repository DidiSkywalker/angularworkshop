import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {MatListModule} from '@angular/material/list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TodoListComponent} from './components/todo-list/todo-list.component';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {ListPageComponent} from './pages/list-page/list-page.component';
import {AddFormComponent} from './components/add-form/add-form.component';
import {AppBarComponent} from './components/app-bar/app-bar.component';
import { TodoListItemComponent } from './components/todo-list-item/todo-list-item.component';
import { DeleteButtonComponent } from './components/delete-button/delete-button.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    HomePageComponent,
    ListPageComponent,
    AddFormComponent,
    AppBarComponent,
    TodoListItemComponent,
    DeleteButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatListModule,
    MatCheckboxModule,
    MatTabsModule,
    MatToolbarModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
